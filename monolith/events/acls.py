import requests
import json
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY

def get_photo(city, state):
    url = "https://api.pexels.com/v1/search"
    payload = {
        "query": f"{city} {state}",
        "per_page": 1
        }
    headers = {"Authorization": PEXELS_API_KEY}
    response = requests.get(url, params=payload, headers=headers)
    content = json.loads(response.content)
    print(content["photos"][0]["src"]["original"], "-----------------")
    try:
        return {"image": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}

def get_weather(city, state):
    geo_url = "http://api.openweathermap.org/geo/1.0/direct"
    geo_params = {"q": city + "," + state + ","+ "US", "limit": 1, "appid": OPEN_WEATHER_API_KEY}
    # headers = {"Authorization": OPEN_WEATHER_API_KEY}
    geo_response = requests.get(geo_url, params=geo_params)
    content = json.loads(geo_response.content)
    lat_lon = {"lat": str(content[0]["lat"]), "lon": str(content[0]["lon"])}

    weather_url = "https://api.openweathermap.org/data/2.5/weather"
    weather_params = {"lat": lat_lon["lat"], "lon": lat_lon["lon"], "units": "imperial", "appid": OPEN_WEATHER_API_KEY}
    # headers = {"Authorization": OPEN_WEATHER_API_KEY}
    weather_response = requests.get(weather_url, params=weather_params)
    content = json.loads(weather_response.content)
    return {"temperature": content["main"]["temp"], "description": content["weather"][0]["description"]}
